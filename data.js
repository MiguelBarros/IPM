var musictitles = ["The Chain", "Creep", "Alive", "Music 4", "Music 5"];

var artists = ["Fleetwood Mac", "Radiohead", "Pearl Jam", "Artist 4", "Artist 5"];

var names = ["food", "map", "music"];

var gradients = ["linear-gradient(#002e4d, #0086e6)", "linear-gradient( #737373, #262626)"];

var keypressed = {
  65: false,
  87: false,
  68: false,
  83: false
};

var personPosition = {
  "top": 75,
  "left": 125
};

var stagecoords = [{
    "top": "5pt",
    "left": "87pt",
  },
  {
    "top": "80pt",
    "left": "12pt",
  },
  {
    "top": "125pt",
    "left": "145pt",
  }
];

var strangerscoords = [{
    "top": "14pt",
    "left": "87pt",
  },
  {
    "top": "3pt",
    "left": "79pt",
  },
  {
    "top": "140pt",
    "left": "8pt",
  },
  {
    "top": "120pt",
    "left": "21pt",
  },
  {
    "top": "14pt",
    "left": "145pt",
  },
  {
    "top": "60pt",
    "left": "21pt",
  },
  {
    "top": "127pt",
    "left": "50pt",
  },
  {
    "top": "78pt",
    "left": "25pt",
  },
  {
    "top": "59pt",
    "left": "45pt",
  },
  {
    "top": "99pt",
    "left": "66pt",
  },
  {
    "top": "127pt",
    "left": "110pt",
  },
  {
    "top": "88pt",
    "left": "15pt",
  },
  {
    "top": "40pt",
    "left": "55pt",
  }

];

var wccoords = [{
    "top": "0pt",
    "left": "35pt",
  },
  {
    "top": "125pt",
    "left": "17pt",
  },
  {
    "top": "47pt",
    "left": "166pt",
  }
];

var friendcoords = [{
    "top": "35pt",
    "left": "125pt",
  },
  {
    "top": "30pt",
    "left": "110pt",
  },
  {
    "top": "100pt",
    "left": "25pt",
  },
  {
    "top": "95pt",
    "left": "46pt",
  },
  {
    "top": "130pt",
    "left": "25pt",
  },
  {
    "top": "100pt",
    "left": "205pt",
  }
];

function getData() {
  return screendata.screens;
}

var screendata = {

  screens: [{
      "lastscreen": -1,
      "title": "Choose Your Order",
      "buttons": ["Burger 1.00€", "Drink 0.50€", "Menu 1.50€"]
    },

    {
      "lastscreen": -1,
      "title": "Burger",
      "buttons": ["Beef", "Chicken", "Vegan"]
    },

    {
      "lastscreen": -1,
      "title": "Drink",
      "buttons": ["Beer", "Soda", "Water", "Tea", "Coffee"]
    },

    {
      "lastscreen": -1,
    }
  ]
}

var index = 0;
var flag = 0;

var currentScreen = 0;
var quantity = 1;

var currentOrder = {
  type: "",
  quantity: "",
  miliseconds: "",
  state: ""
};
var orderHistory = [];

var mousepositionBegin = null;

var foodMenuButton = null;
var foodMenuIndex = 0;

var musicMenuButton = null;
var musicMenuIndex = 0;

var mapDisplay = 0;
var mapFood = 0;

var helpFlag = 0;
var noteFlag = 0;

/*#############################################################################
#                              Functions                                      #
#############################################################################*/

function toogleHelp() {
  window.helpFlag = (window.helpFlag + 1) % 2;

  if (window.helpFlag) {
    document.getElementById("helpIMG").style.background = "linear-gradient(#002e4d, #0086e6)";
    var para = document.createElement("P");
    para.setAttribute("id", "help");
    var t;
    document.body.appendChild(para);
    para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');
    t = document.createTextNode('HELP MODE: Click on the button to know its function');
    para.appendChild(t);
    document.body.appendChild(para);
    window.noteFlag = 1;
    para.setAttribute('onclick', 'window.noteFlag = 0; document.body.removeChild(document.getElementById("help"));');
  } else {
    if (document.getElementById("help") != null) {
      document.body.removeChild(document.getElementById("help"));
    }
    document.getElementById("helpIMG").style.background = " rgba(255, 255, 255, 0.2)";
  }
}

function writeSong() {

  if (flag == 0) {
    document.getElementById("circle0").style.stroke = "white";
    flag = 1;
  }

  var index = Math.floor((Math.random() * 4) + 1);

  var song = artists[index] + " - " + musictitles[index];

  document.getElementById("song").innerHTML = song;

  setTimeout(writeSong, 180000);
}


function getTime() {
  var date = new Date();

  var h = date.getHours();
  if (h < "10") h = "0" + h;

  var m = date.getMinutes();
  if (m < "10") m = "0" + m;

  document.getElementById("hour").innerHTML = h + ":" + m;

  setTimeout(getTime, 1000);

}

function mousedownfunct(e) {

  mousepositionBegin = [e.clientX, e.clientY];
}

function mouseupfunct(e) {
  var currentmouse = [e.clientX, e.clientY];

  var distanceX = Math.pow(currentmouse[0] - mousepositionBegin[0], 2);

  var distanceY = Math.pow(currentmouse[1] - mousepositionBegin[1], 2);

  var distance = Math.pow(distanceX + distanceY, 0.5);

  if (distance >= 10) {
    if (currentmouse[0] > mousepositionBegin[0]) {
      changeImagePrev();
      return;
    } else if (currentmouse[0] < mousepositionBegin[0]) {
      changeImageNext();
      return;
    }
  }
}


function changeImageNext() {

  var circleid = "circle" + index + "";

  document.getElementById(circleid).style.stroke = "";
  index = (index + 1) % 3;

  circleid = "circle" + index + "";

  document.getElementById(circleid).style.stroke = "white";

  document.getElementById("menu-image").src = names[index] + ".png";
  document.getElementById("menu-image").name = names[index];
}


function changeImagePrev() {
  var circleid = "circle" + index + "";

  document.getElementById(circleid).style.stroke = "";

  index = index - 1;
  if (index == -1) index = 2;

  circleid = "circle" + index + "";

  document.getElementById(circleid).style.stroke = "white";

  document.getElementById("menu-image").src = names[index] + ".png";
  document.getElementById("menu-image").name = names[index];

}


function doFunctionality() {

  var name = document.getElementById("menu-image").name;

  if (name === "food") {
    window.location.href = "food-menu.html";
  } else if (name === "music") {
    window.location.href = "music-funct.html";
  } else if (name === 'map')
    window.location.href = 'mapfunct.html';
}

/*#############################################################################
#                          Food Functionality                                 #
#############################################################################*/

function loadAllSreenData(screenData) {
  sessionStorage.setItem("AllScreenData", JSON.stringify(screenData));
}

function writeSreenWithIndex(index) {

  var p;
  var div = document.getElementById('chooseMenu');

  //pega nos elementos dentro da div e remove-as (apaga o ecra)
  while (div.hasChildNodes()) {
    div.removeChild(div.lastChild);
  }

  currentScreen = index;
  screenData = JSON.parse(sessionStorage.getItem("AllScreenData"));
  document.getElementById('underline').innerHTML = screenData[index].title;

  var b = screenData[index].buttons;

  for (var i = 0; i < b.length; i++) {
    p = document.createElement("p");
    p.setAttribute("class", "food-button");
    p.setAttribute("id", "button" + i + "");
    p.setAttribute("onmousedown", "selectedButton(this,0)");
    p.setAttribute("onmouseup", "nextScreen()");
    p.setAttribute("onmouseout", "selectedButton(this,1)")
    var node = document.createTextNode(b[i]);
    p.appendChild(node);

    div.appendChild(p);
  }

  document.getElementById('prev-image').style.visibility = 'visible';

  if (b.length < 4)
    div.style.overflowY = "hidden";
  else
    div.style.overflowY = "scroll";
}

function selectedButton(button, index) {

  button.style.background = gradients[index];

  if (currentScreen == 0) {
    createNewOrder(button.innerHTML);
  } else if (currentScreen == 1) {
    addToOrder(button.innerHTML);
  } else {
    addToOrder(button.innerHTML);
  }
}

function createNewOrder(orderType) {

  currentOrder.burger = null;
  currentOrder.drink = null;

  if (orderType === "Menu 1.50€") {
    currentOrder.type = "Menu";
    currentOrder.price = 1.50;
  } else if (orderType === "Drink 0.50€") {
    currentOrder.type = "Drink";
    currentOrder.price = 0.50;
  } else if (orderType === "Burger 1.00€") {
    currentOrder.type = "Burger";
    currentOrder.price = 1.00;
  }
}

function addToOrder(string) {

  if (currentScreen == 1) {
    currentOrder.burger = string;
  } else if (currentScreen == 2) {
    currentOrder.drink = string;
  }
}

function nextScreen() {

  var number;
  screenData = JSON.parse(sessionStorage.getItem("AllScreenData"));

  if (currentScreen == 0) {
    if (currentOrder.type === "Burger" || currentOrder.type === "Menu") {
      number = 1;
    } else if (currentOrder.type === "Drink") {
      number = 2;
    }
  } else if (currentScreen == 1) {
    if (currentOrder.type === "Burger") {
      screenData[3].lastscreen = currentScreen;
      sessionStorage.setItem("AllScreenData", JSON.stringify(screenData));
      writePayScreen();
      return;
    } else if (currentOrder.type === "Menu") {
      number = 1;
    }
  } else {
    screenData[3].lastscreen = currentScreen;
    sessionStorage.setItem("AllScreenData", JSON.stringify(screenData));
    writePayScreen();
    return;
  }

  screenData[currentScreen + number].lastscreen = currentScreen;
  sessionStorage.setItem("AllScreenData", JSON.stringify(screenData));
  writeSreenWithIndex(currentScreen + number);
}

function prevScreen() {

  if (currentScreen == 5 || currentScreen == 0) {
    window.sessionStorage.removeItem("AllScreenData");
    window.location.href = "food-menu.html";
    return;
  } else if (currentScreen == 4) {
    document.getElementById("fingerScan").style.display = "none";
    document.getElementById("check").style.display = "block";
    currentScreen = 3;
    return;
  } else if (currentScreen == 3) {
    document.getElementById("payScreen").style.zIndex = -1;
    document.getElementById("selectScreen").style.zIndex = 1;
  }

  screenData = JSON.parse(sessionStorage.getItem("AllScreenData"));
  var index = screenData[currentScreen].lastscreen;
  screenData[currentScreen].lastscreen = -1;
  sessionStorage.setItem("AllScreenData", JSON.stringify(screenData));
  writeSreenWithIndex(index);
}

function clearScreen() {

  var table, row;

  document.getElementById("orderType").innerHTML = "";
  document.getElementById("quantity").innerHTML = "";
  document.getElementById("totalPrice").innerHTML = "";

  table = document.getElementById('descriptionTable');
  for (var i = 1; i < 3; i++) {
    row = document.getElementById('row' + i + '');
    if (row != null) {
      table.deleteRow(row.rowIndex);
    }
  }
}

function writePayScreen() {

  var table, row, cell0, cell1;
  var insertNumber = 1;

  document.getElementById("selectScreen").style.zIndex = -1;
  document.getElementById("payScreen").style.zIndex = 1;

  clearScreen();

  if (quantity == 1)
    document.getElementById("minus").style.visibility = "hidden";

  currentScreen = 3;

  document.getElementById("orderType").innerHTML = currentOrder.type;
  document.getElementById("quantity").innerHTML = quantity;

  table = document.getElementById('descriptionTable');
  if (currentOrder.burger != null) {
    row = table.insertRow(insertNumber);
    row.setAttribute("id", "row" + insertNumber + "");
    cell0 = row.insertCell(0);
    cell0.style.paddingLeft = "5pt";
    cell0.innerHTML = currentOrder.burger;
    cell1 = row.insertCell(1);
    cell1.setAttribute("class", "price");
    cell1.innerHTML = "1.00&euro;"
    insertNumber++;
  }

  if (currentOrder.drink != null) {
    row = table.insertRow(insertNumber);
    row.setAttribute("id", "row" + insertNumber + "");
    cell0 = row.insertCell(0);
    cell0.style.paddingLeft = "5pt";
    cell0.innerHTML = currentOrder.drink;
    cell1 = row.insertCell(1);
    cell1.setAttribute("class", "price");
    cell1.innerHTML = "0.50&euro;"
  }

  var total = currentOrder.price * quantity;
  document.getElementById('totalPrice').innerHTML = "" + total.toFixed(2) + "&euro;"
}

function changeQuantity(n) {

  quantity = parseInt(document.getElementById('quantity').innerHTML) + n;

  if (quantity > 1) {
    document.getElementById('minus').style.visibility = "visible";
  } else {
    document.getElementById('minus').style.visibility = "hidden";
  }

  writePayScreen();
}


function payButton() {

  document.getElementById("check").style.display = "none";
  document.getElementById("fingerScan").style.display = "block";
  currentScreen = 4;
}


function fingerScan() {

  orderHistory = JSON.parse(sessionStorage.getItem("orderHistory"));
  if (orderHistory == null)
    orderHistory = [];

  currentOrder.quantity = quantity.toString();

  var current = new Date();
  currentOrder.miliseconds = current.getTime();

  currentOrder.state = "Preparing";
  orderHistory.splice(0, 0, currentOrder);
  // orderHistory.push(currentOrder);

  sessionStorage.setItem("orderHistory", JSON.stringify(orderHistory));
  document.getElementById("fingerScan").style.display = "none";
  document.getElementById("waitScreen").style.display = "block";

  currentScreen = 5;

  var bar = document.getElementById('progress1');
  updateProgressBar(bar, null);
}

function updateProgressBar(bar, change) {

  if (bar.value < bar.max) {
    bar.value++;
    setTimeout(function() {
      updateProgressBar(bar);
    }, 1000);
  } else if (bar.value == bar.max && change != null) {
    var image = document.createElement("IMG");
    document.getElementById('changeable').appendChild(image);
  }
}

function confirmCancel(string) {

  if (string === "cancel") {
    document.getElementById('confirm').style.display = "block";
  } else if (string === "no") {
    document.getElementById('confirm').style.display = "none";
  } else {
    window.location.reload();
  }
}

function doMenuButtons(n, index) {

  window.foodMenuButton = n;
  document.getElementById("button" + foodMenuButton + "").style.background = gradients[index];
}

function doHelp(n) {

  var para = document.createElement("P");
  para.setAttribute("id", "help");
  var t;
  document.body.appendChild(para);
  para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');

  if (n == 0)
    t = document.createTextNode('Choose your order, pay and wait for it to arrive');

  else if (n == 1)
    t = document.createTextNode('View the current state of your orders');

  else if (n == 2)
    t = document.createTextNode('See how far your orders are from your position');

  para.appendChild(t);
  document.body.appendChild(para);

  window.noteFlag = 1;

  para.setAttribute('onclick', 'window.noteFlag = 0; document.body.removeChild(document.getElementById("help"));');
}

function goToNext() {

  if (foodMenuButton == 0) {
    if (window.helpFlag)
      doHelp(0);
    else
      window.location.href = "food-funct.html";
  } else if (foodMenuButton == 1 || foodMenuButton == 2) {
    if (window.helpFlag) {
      doHelp(window.foodMenuButton);
    } else {
      foodMenuIndex = 1;
      document.getElementById("chooseMenu").style.display = "none";
      document.getElementById("underline").innerHTML = "Orders";
      document.body.removeChild(document.getElementById('helpIMG'));
      if (foodMenuButton == 1)
        writeOrders();
      else
        writeOrdersComplete();
    }
  }
}

function backButtonMenu() {

  if (foodMenuIndex == 0) {
    if (window.helpFlag) {
      if (window.noteFlag) {
        window.noteFlag = 0;
        document.body.removeChild(document.getElementById("help"));
      } else {
        window.helpFlag = 0;
        document.getElementById("helpIMG").style.background = " rgba(255, 255, 255, 0.2)";
      }
    } else {
      window.location.href = "screen.html";
    }
  } else if (foodMenuIndex == 1) {
    window.location.href = "food-menu.html";
  }
}

function createNote() {

  var d, p, t;

  d = document.createElement("DIV");
  d.setAttribute("id", "notification");

  p = document.createElement("P");
  p.style.margin = 0;
  t = document.createTextNode('Your order is ready. Click');
  p.appendChild(t);
  d.appendChild(p);

  var x = document.createElement("A");
  x.style.color = "rgb(255,255,255)";
  x.onclick = function() {
    sessionStorage.setItem("notificationFlag", "1");
  };
  t = document.createTextNode("HERE");
  x.setAttribute("href", "food-menu.html");
  x.appendChild(t);
  d.appendChild(x);

  p = document.createElement("P");
  p.style.margin = 0;
  t = document.createTextNode('to see details');
  p.appendChild(t);
  d.appendChild(p);

  document.body.appendChild(d);

  d.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');

  setTimeout(function() {
    document.body.removeChild(d);
  }, 5000);
}

function createNotification() {

  var img = document.createElement("img");
  img.src = 'notification.png';
  img.id = 'notificationIMG';
  img.setAttribute("onclick", "createNote()");
  document.body.appendChild(img);

  setTimeout(function() {
    document.body.removeChild(img);
  }, 5000);
}

function checkFlag() {

  var f = sessionStorage.getItem("notificationFlag");
  if (f == null) {
    return;
  } else {
    sessionStorage.removeItem("notificationFlag");
    window.foodMenuButton = 2;
    goToNext();
  }
}

function checkOrders() {

  var food = JSON.parse(sessionStorage.getItem("orderHistory"));

  if (food == null || food.length == 0) {
    setTimeout(checkOrders, 1000);
    return;
  }

  var d = new Date();
  var current = d.getTime();
  for (var i = 0; i < food.length; i++) {

    if (current - food[i].miliseconds >= 30000 && food[i].state != "Delivering") {
      food[i].state = "Delivering";
      createNotification();
    } else if (current - food[i].miliseconds > 1800000) {
      food.splice(i, 1);
    }
  }

  sessionStorage.setItem("orderHistory", JSON.stringify(food));
  setTimeout(checkOrders, 1000);
  return;
}

function goToMap(d) {

  var div, pin;

  div = document.getElementById("map");

  if (window.mapDisplay == 0) {
    d.style.opacity = 0.5;
    window.mapDisplay = 1;
    eventManager();
    div.style.visibility = "visible";
    div.classList.remove('hide');
    div.classList.add('show');
    pin = document.createElement("IMG");
    pin.src = "map.png";
    pin.setAttribute("class", "food-pin");
    pin.setAttribute("id", "pin" + d.id)
    var a = Math.floor((Math.random() * 20) + 1);
    pin.style.top = 30 + 30 * d.id + a + "pt";
    pin.style.left = 9 + 30 * d.id + a + "pt";

    div.appendChild(pin);
    window.mapFood++;
  } else {

    pin = document.getElementById("pin" + d.id);

    if (pin == null) {
      d.style.opacity = 0.5;
      pin = document.createElement("IMG");
      pin.src = "map.png";
      pin.setAttribute("class", "food-pin");
      pin.setAttribute("id", "pin" + d.id)
      var a = Math.floor((Math.random() * 20) + 1);
      pin.style.top = 30 + 7 * d.id + a + "pt";
      pin.style.left = 9 + 7 * d.id + a + "pt";
      div.appendChild(pin);
      window.mapFood++;
    } else {
      d.style.opacity = 1;
      div.removeChild(pin);
      window.mapFood--;

      if (window.mapFood == 0) {
        window.mapDisplay = 0;
        eventManager();
        div.classList.remove('show');
        div.classList.add('hide');
        setTimeout(function() {
          div.style.visibility = 'hidden';
        }, 1000);
      }
    }
  }
}

function writeOrders() {

  var date, current, row, cell0, cell1, cell2;
  var food = JSON.parse(sessionStorage.getItem("orderHistory"));

  if (food == null) return;

  var div = document.getElementById("table");
  while (div.hasChildNodes()) {
    div.removeChild(div.lastChild);
  }

  var table = document.createElement("TABLE");
  table.style.width = "100%";
  table.style.borderCollapse = "collapse";
  table.style.textAlign = "left";

  for (var i = 0; i < food.length; i++) {

    row = table.insertRow();
    cell0 = row.insertCell(0);
    cell1 = row.insertCell(1);
    cell2 = row.insertCell(2);
    cell0.innerHTML = "Type:";
    cell1.innerHTML = food[i].type;
    cell2.setAttribute("id", "changeable");

    date = new Date();
    current = date.getTime();

    var bar = document.createElement("PROGRESS");
    bar.value = (current - food[i].miliseconds) / 1000;
    bar.max = "30";
    bar.setAttribute("id", "progress2");
    cell2.appendChild(bar);

    if (food[i].state === "Preparing") {
      updateProgressBar(bar);
    } else if (food[i].state === "Delivering") {
      cell2.innerHTML = "<img src='" + "map.png" + "' width='12pt'>";
      cell2.setAttribute("id", i);
      cell2.setAttribute("onclick", "goToMap(this)")
    }

    row = table.insertRow();
    cell0 = row.insertCell(0);
    cell1 = row.insertCell(1);
    cell2 = row.insertCell(2);
    cell0.style.width = "5pt";
    cell1.innerHTML = "Quantity:";
    cell2.innerHTML = food[i].quantity;

    if (food[i].burger != null) {
      row = table.insertRow();
      cell0 = row.insertCell(0);
      cell1 = row.insertCell(1);
      cell2 = row.insertCell(2);
      cell0.style.width = "5pt";
      cell1.innerHTML = "Burger:";
      cell2.innerHTML = food[i].burger;
    }

    if (food[i].drink != null) {
      row = table.insertRow();
      cell0 = row.insertCell(0);
      cell1 = row.insertCell(1);
      cell2 = row.insertCell(2);
      cell0.style.width = "5pt";
      cell1.innerHTML = "Drink:";
      cell2.innerHTML = food[i].drink;
    }

    row = table.insertRow();
    for (var j = 0; j < 3; j++) {
      cell0 = row.insertCell(j);
      cell0.style.borderTopStyle = "solid";
      cell0.style.borderTopColor = "white	";
      cell0.style.borderTopWidth = "1pt";
    }
  }

  div.appendChild(table);
}

function writeOrdersComplete() {

  var row, cell0, cell1, cell2;
  var food = JSON.parse(sessionStorage.getItem("orderHistory"));

  if (food == null) return;

  var div = document.getElementById("table");
  while (div.hasChildNodes()) {
    div.removeChild(div.lastChild);
  }

  var table = document.createElement("TABLE");
  table.style.width = "100%";
  table.style.borderCollapse = "collapse";
  table.style.textAlign = "left";

  for (var i = food.length - 1; i >= 0; i--) {
    if (food[i].state === "Delivering") {
      row = table.insertRow();
      cell0 = row.insertCell(0);
      cell1 = row.insertCell(1);
      cell2 = row.insertCell(2);
      cell0.innerHTML = "Type:";
      cell1.innerHTML = food[i].type;
      cell2.setAttribute("id", "changeable");
      cell2.innerHTML = "<img src='" + "map.png" + "' width='12pt'>";
      cell2.setAttribute("id", i);
      cell2.setAttribute("onclick", "goToMap(this)")

      row = table.insertRow();
      cell0 = row.insertCell(0);
      cell1 = row.insertCell(1);
      cell2 = row.insertCell(2);
      cell0.style.width = "5pt";
      cell1.innerHTML = "Quantity:";
      cell2.innerHTML = food[i].quantity;

      if (food[i].burger != null) {
        row = table.insertRow();
        cell0 = row.insertCell(0);
        cell1 = row.insertCell(1);
        cell2 = row.insertCell(2);
        cell0.style.width = "5pt";
        cell1.innerHTML = "Burger:";
        cell2.innerHTML = food[i].burger;
      }

      if (food[i].drink != null) {
        row = table.insertRow();
        cell0 = row.insertCell(0);
        cell1 = row.insertCell(1);
        cell2 = row.insertCell(2);
        cell0.style.width = "5pt";
        cell1.innerHTML = "Drink:";
        cell2.innerHTML = food[i].drink;
      }

      row = table.insertRow();
      for (var j = 0; j < 3; j++) {
        cell0 = row.insertCell(j);
        cell0.style.borderTopStyle = "solid";
        cell0.style.borderTopColor = "white	";
        cell0.style.borderTopWidth = "1pt";
      }
    }
  }

  div.appendChild(table);
}

function eventManager() {

  if (window.mapDisplay) {
    window.addEventListener('keyup', keyUp);
    window.addEventListener('keydown', keyDown);
  } else {
    window.removeEventListener('keyup', keyUp);
    window.removeEventListener('keydown', keyDown);
  }
}

function keyUp(event) {

  if (!(event.keyCode in keypressed)) {
    return;
  } else {
    keypressed[event.keyCode] = false;
  }
}

function keyDown(event) {

  var keycode = event.keyCode;
  var img = document.getElementById('person');

  if (!(event.keyCode in keypressed)) {
    return;
  }

  keypressed[event.keyCode] = true;

  if (keypressed[87] && keypressed[65]) {
    img.style.transform = 'rotate(-90deg)'; /* W & A */
    moveDiagonal(img, -1, -1);
  } else if (keypressed[87] && keypressed[68]) {
    img.style.transform = 'rotate(0deg)'; /* W & D */
    moveDiagonal(img, -1, 1);
  } else if (keypressed[83] && keypressed[65]) {
    img.style.transform = 'rotate(180deg)'; /* S & A */
    moveDiagonal(img, 1, -1);
  } else if (keypressed[83] && keypressed[68]) {
    img.style.transform = 'rotate(90deg)'; /* S & D*/
    moveDiagonal(img, 1, 1);
  } else if (keypressed[65]) {
    img.style.transform = 'rotate(-135deg)'; /* A */
    moveHorizontal(img, -1);
  } else if (keypressed[87]) {
    img.style.transform = 'rotate(-45deg)'; /* W */
    moveVertical(img, -1);
  } else if (keypressed[68]) {
    img.style.transform = 'rotate(45deg)'; /* D */
    moveHorizontal(img, 1);
  } else if (keypressed[83]) {
    img.style.transform = 'rotate(135deg)'; /* S */
    moveVertical(img, 1);
  }
}

function moveHorizontal(element, move) {

  var tmp = personPosition.left + move;

  personPosition.left = tmp;

  if (tmp >= 5 && tmp <= 235) {
    element.style.left = '' + tmp + 'pt';
  }
}

function moveVertical(element, move) {

  var tmp = personPosition.top + move;

  personPosition.top = tmp;

  if (tmp >= 10 && tmp <= 130) {
    element.style.top = '' + tmp + 'pt';
  }
}

function moveDiagonal(element, top, left) {

  var tmp1 = personPosition.top + top;
  var tmp2 = personPosition.left + left;

  personPosition.top = tmp1;
  personPosition.left = tmp2;

  if (tmp1 >= 10 && tmp1 <= 130 && tmp2 >= 5 && tmp2 <= 235) {
    element.style.top = '' + tmp1 + 'pt';
    element.style.left = '' + tmp2 + 'pt';
  }
}

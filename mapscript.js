var mapMenuButton = null;
var mapMenuIndex = 0;

var mapDisplay = 0;
var mapStages = 0;

var mapWC = 0;

var friendArray = [0, 0, 0, 0, 0, 0];
var strangersArray = [0, 0, 0, 0];

var stragerscount = 0;
var friendcount = 0;

/*****************************************************************************
|                             Functions                                      |
*****************************************************************************/

function toogleHelp() {
  window.helpFlag = (window.helpFlag + 1) % 2;

  if (window.helpFlag) {
    document.getElementById("helpIMG3").style.background = "linear-gradient(#002e4d, #0086e6)";
    var para = document.createElement("P");
    para.setAttribute("id", "help");
    var t;
    document.body.appendChild(para);
    para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');
    t = document.createTextNode('HELP MODE: Click on the button to know its function');
    para.appendChild(t);
    document.body.appendChild(para);
    window.noteFlag = 1;
    para.setAttribute('onclick', 'window.noteFlag = 0; document.body.removeChild(document.getElementById("help"));');
  } else {
    if (document.getElementById("help") != null) {
      document.body.removeChild(document.getElementById("help"));
    }
    document.getElementById("helpIMG3").style.background = " rgba(255, 255, 255, 0.2)";
  }
}


function doHelp(n) {
  var para = document.createElement("P");
  para.setAttribute("id", "help");
  var t;
  document.body.appendChild(para);

  para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');


  if (n == 0)
    t = document.createTextNode('Locate your friends on the map');

  else if (n == 1)
    t = document.createTextNode('Locate every stage in the festival');

  else if (n == 2)
    t = document.createTextNode('Locate bathrooms in the festival');

  else if (n == 3)
    t = document.createTextNode('Find stranger with similar interests');


  para.appendChild(t);
  document.body.appendChild(para);

  window.noteFlag = 1;

  para.setAttribute('onclick', 'window.noteFlag = 0; document.body.removeChild(document.getElementById("help"));');

}

function clickFriend(n) {

  var div, pin;

  div = document.getElementById("map");

  if (window.friendArray[n] == 0) {

    if (window.mapDisplay == 0) {
      window.mapDisplay = 1;
      div.style.visibility = "visible";
      div.classList.remove('hide');
      div.classList.add('show');
    }

    window.friendcount++;
    window.friendArray[n] = 1
    document.getElementById("friend" + n).style.opacity = 0.5;
    pin = document.createElement("IMG");
    pin.src = "friend" + n + ".png";
    pin.setAttribute("class", "f-pin");
    pin.setAttribute("id", "fpin" + n);
    pin.style.top = friendcoords[n].top;
    pin.style.left = friendcoords[n].left;

    div.appendChild(pin);
  } else {
    window.friendArray[n] = 0
    window.friendcount--;
    div.removeChild(document.getElementById("fpin" + n));
    document.getElementById("friend" + n).style.opacity = 1;
    if (window.mapStages == 0 && window.mapWC == 0 && window.friendcount == 0) {
      window.mapDisplay = 0;
      div.classList.remove('show');
      div.classList.add('hide');
      setTimeout(function() {
        div.style.visibility = 'hidden';
      }, 1000);
    }
  }
}

function changeColor(n, index) {

  window.mapMenuButton = n;
  document.getElementById("button" + mapMenuButton + "").style.background = gradients[index];

}

function backButton() {

  if (window.mapMenuIndex == 1) {
    document.getElementById("selectScreen").style.display = "block";
    document.getElementById("friendScreen").style.display = "none";
    var img = document.createElement("IMG");
    img.setAttribute('onclick', 'toogleHelp()');
    img.setAttribute('class', 'helpimg');
    img.id = 'helpIMG3';
    img.src = 'information.png';
    document.body.appendChild(img);
    window.mapMenuIndex = 0;

  } else if (window.mapMenuIndex == 2) {
    document.getElementById("selectScreen").style.display = "block";
    document.getElementById("strangerDanger").style.display = "none";
    var img = document.createElement("IMG");
    img.setAttribute('onclick', 'toogleHelp()');
    img.setAttribute('class', 'helpimg');
    img.id = 'helpIMG3';
    img.src = 'information.png';
    document.body.appendChild(img);
    window.mapMenuIndex = 0;

  } else if (window.mapMenuIndex == 10) {
    document.getElementById("selectScreen").style.display = "block";
    document.getElementById("stages").style.display = "none";
    var img = document.createElement("IMG");
    img.setAttribute('onclick', 'toogleHelp()');
    img.setAttribute('class', 'helpimg');
    img.id = 'helpIMG3';
    img.src = 'information.png';
    document.body.appendChild(img);
    window.mapMenuIndex = 0;

  } else if (window.mapMenuIndex == 0) {
    if (window.helpFlag) {
      if (window.noteFlag) {
        window.noteFlag = 0;
        document.body.removeChild(document.getElementById("help"));
      } else {
        window.helpFlag = 0;
        document.getElementById("helpIMG3").style.background = " rgba(255, 255, 255, 0.2)";
      }
    } else {
      window.location.href = "screen.html";
    }
  }
}

function goToStrangers() {
  document.getElementById("selectScreen").style.display = "none";
  document.getElementById("strangerDanger").style.display = "block";
  document.body.removeChild(document.getElementById('helpIMG3'));
  window.mapMenuIndex = 2;
}

function clickButton(button) {

  button.style.background = "linear-gradient( #737373, #262626)";
  if (window.mapMenuButton == 0) {
    if (window.helpFlag)
      doHelp(0);
    else {
      document.getElementById("selectScreen").style.display = "none";
      document.getElementById("friendScreen").style.display = "block";
      document.body.removeChild(document.getElementById('helpIMG3'));
      window.mapMenuIndex = 1;
    }

  } else if (window.mapMenuButton == 1) {
    if (window.helpFlag)
      doHelp(1);
    else
      showStages();
  } else if (window.mapMenuButton == 2) {
    if (window.helpFlag)
      doHelp(2);
    else {
      toggleMap(showRest);
    }
  } else if (window.mapMenuButton == 3) {
    if (window.helpFlag)
      doHelp(3);
    else {
      goToStrangers();
    }
  }
}


function showRest() {

  var div, pin;

  div = document.getElementById("map");

  if (window.mapWC == 1) {

    window.mapWC = 0;

    var imgs = document.getElementsByClassName("wc-pin");

    while (imgs.length > 0) {
      div.removeChild(imgs[0]);
    }
  } else {
    window.mapWC = 1;

    for (var i = 0; i < 3; i++) {

      pin = document.createElement("IMG");
      pin.src = "WCPin.png";
      pin.setAttribute("class", "wc-pin");
      pin.style.top = wccoords[i].top;
      pin.style.left = wccoords[i].left;

      div.appendChild(pin);
    }
  }
  return;
}

function showStage(image, number) {

  var pin;
  var div = document.getElementById('map');

  if (image.style.opacity == 1 || image.style.opacity == "") {
    if (window.mapDisplay == 0) {
      window.mapDisplay = 1;
      div.style.visibility = "visible";
      div.classList.remove('hide');
      div.classList.add('show');
    }
    image.style.opacity = 0.5;
    window.mapStages++;
    pin = document.createElement("IMG");
    pin.src = image.src;
    pin.id = number;
    pin.setAttribute("class", "stage-pin");
    pin.style.top = stagecoords[number].top;
    pin.style.left = stagecoords[number].left;

    div.appendChild(pin);
  } else {
    window.mapStages--;
    if (window.mapStages == 0 && window.mapWC == 0 && window.friendcount == 0) {
      window.mapDisplay = 0;
      div.classList.remove('show');
      div.classList.add('hide');
      setTimeout(function() {
        div.style.visibility = 'hidden';
      }, 1000);
    }
    image.style.opacity = 1;
    pin = document.getElementById('' + number + '');
    div.removeChild(pin);
  }
}

function showStages() {

  document.getElementById("selectScreen").style.display = "none";
  document.getElementById("stages").style.display = "block";
  document.body.removeChild(document.getElementById('helpIMG3'));
  window.mapMenuIndex = 10;
}

function toggleMap(functionCall) {

  var div = document.getElementById('map');

  if (window.mapDisplay == 0) {
    window.mapDisplay = 1;
    div.style.visibility = "visible";
    div.classList.remove('hide');
    div.classList.add('show');
    functionCall();
  } else {
    functionCall();
    if (window.mapStages == 0 && window.mapWC == 0 && window.friendcount == 0) {
      window.mapDisplay = 0;
      div.classList.remove('show');
      div.classList.add('hide');
      setTimeout(function() {
        div.style.visibility = 'hidden';
      }, 1000);
    }
  }
}

function eventManager() {

  if (window.mapDisplay) {
    window.addEventListener('keyup', keyUp);
    window.addEventListener('keydown', keyDown);
  } else {
    window.removeEventListener('keyup', keyUp);
    window.removeEventListener('keydown', keyDown);
  }

  setTimeout(eventManager, 1000);
}

function keyUp(event) {

  if (!(event.keyCode in keypressed)) {
    return;
  } else {
    keypressed[event.keyCode] = false;
  }
}

function keyDown(event) {

  var keycode = event.keyCode;
  var img = document.getElementById('person');

  if (!(event.keyCode in keypressed)) {
    return;
  }

  keypressed[event.keyCode] = true;

  if (keypressed[87] && keypressed[65]) {
    img.style.transform = 'rotate(-90deg)'; /* W & A */
    moveDiagonal(img, -1, -1);
  } else if (keypressed[87] && keypressed[68]) {
    img.style.transform = 'rotate(0deg)'; /* W & D */
    moveDiagonal(img, -1, 1);
  } else if (keypressed[83] && keypressed[65]) {
    img.style.transform = 'rotate(180deg)'; /* S & A */
    moveDiagonal(img, 1, -1);
  } else if (keypressed[83] && keypressed[68]) {
    img.style.transform = 'rotate(90deg)'; /* S & D*/
    moveDiagonal(img, 1, 1);
  } else if (keypressed[65]) {
    img.style.transform = 'rotate(-135deg)'; /* A */
    moveHorizontal(img, -1);
  } else if (keypressed[87]) {
    img.style.transform = 'rotate(-45deg)'; /* W */
    moveVertical(img, -1);
  } else if (keypressed[68]) {
    img.style.transform = 'rotate(45deg)'; /* D */
    moveHorizontal(img, 1);
  } else if (keypressed[83]) {
    img.style.transform = 'rotate(135deg)'; /* S */
    moveVertical(img, 1);
  }
}

function moveHorizontal(element, move) {

  var tmp = personPosition.left + move;

  personPosition.left = tmp;

  if (tmp >= 5 && tmp <= 235) {
    element.style.left = '' + tmp + 'pt';
  }
}

function moveVertical(element, move) {

  var tmp = personPosition.top + move;

  personPosition.top = tmp;

  if (tmp >= 10 && tmp <= 130) {
    element.style.top = '' + tmp + 'pt';
  }
}

function moveDiagonal(element, top, left) {

  var tmp1 = personPosition.top + top;
  var tmp2 = personPosition.left + left;

  personPosition.top = tmp1;
  personPosition.left = tmp2;

  if (tmp1 >= 10 && tmp1 <= 130 && tmp2 >= 5 && tmp2 <= 235) {
    element.style.top = '' + tmp1 + 'pt';
    element.style.left = '' + tmp2 + 'pt';
  }
}

function goToGroups(n) {
  var div, pin, i;

  div = document.getElementById("map");

  if (window.strangersArray[n] == 0) {
    if (window.mapDisplay == 0) {
      window.mapDisplay = 1;
      div.style.visibility = "visible";
      div.classList.remove('hide');
      div.classList.add('show');
    }

    window.stragerscount++;
    window.strangersArray[n] = 1;
    document.getElementById("group" + n).style.opacity = 0.5;

    for (i = 0; i < 3; i++) {
      pin = document.createElement("IMG");
      pin.src = "group" + n + ".png";
      pin.setAttribute("class", "s-pin");
      pin.setAttribute("id", "spin" + n * 3 + i);
      pin.style.top = strangerscoords[n * 3 + i].top;
      pin.style.left = strangerscoords[n * 3 + i].left;
      div.appendChild(pin);
    }
  } else {
    window.strangersArray[n] = 0
    window.stragerscount--;
    for (i = 0; i < 3; i++) {
      div.removeChild(document.getElementById("spin" + n * 3 + i));
    }
    document.getElementById("group" + n).style.opacity = 1;
    if (window.mapStages == 0 && window.mapWC == 0 && window.friendcount == 0 && window.stragerscount == 0) {
      window.mapDisplay = 0;
      div.classList.remove('show');
      div.classList.add('hide');
      setTimeout(function() {
        div.style.visibility = 'hidden';
      }, 1000);
    }
  }
}

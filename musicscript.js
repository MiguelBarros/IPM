var musicMenuButton = null;
var musicMenuIndex = 0;

var mapDisplay = 0;
var mapStages = 0;

var playlistArray = [0, 0, 0, 0];

var helpFlag = 0;
var noteFlag = 0;

function toogleHelp() {
  window.helpFlag = (window.helpFlag + 1) % 2;

  if (window.helpFlag) {
    document.getElementById("helpIMG2").style.background = "linear-gradient(#002e4d, #0086e6)";
    var para = document.createElement("P");
    para.setAttribute("id", "help");
    var t;
    document.body.appendChild(para);
    para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');
    t = document.createTextNode('HELP MODE: Click on the button to know its function');
    para.appendChild(t);
    document.body.appendChild(para);
    window.noteFlag = 1;
    para.setAttribute('onclick', 'window.noteFlag = 0; document.body.removeChild(document.getElementById("help"));');
  } else {
    if (document.getElementById("help") != null) {
      document.body.removeChild(document.getElementById("help"));
    }
    document.getElementById("helpIMG2").style.background = " rgba(255, 255, 255, 0.2)";
  }
}

function showLyricsStage(n) {
	document.getElementById('stages').style.display = 'none';
	document.getElementById('lyrics' + n).style.display = 'block';
	window.musicMenuIndex = 24;
}

function doHelp(n) {
  var para = document.createElement("P");
  para.setAttribute("id", "help");
  var t;
  document.body.appendChild(para);

  para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');

  if (n == 0)
    t = document.createTextNode('See the lyrics of the music playing on your stage');

  else if (n == 1)
    t = document.createTextNode('See the music playing currently on every stage');

  else if (n == 2)
    t = document.createTextNode('Add the current music to a playlist');

  else if (n == 3)
    t = document.createTextNode('Vote for the next song');


  para.appendChild(t);
  document.body.appendChild(para);

  window.noteFlag = 1;

  para.setAttribute('onclick', 'window.noteFlag = 0; document.body.removeChild(document.getElementById("help"));');
}

function changeColor(n, index) {

  musicMenuButton = n;
  document.getElementById("button" + musicMenuButton + "").style.background = gradients[index];
}

function goBackMusic() {

  /*depois por case*/
  if (musicMenuIndex == 0) {
    if (window.helpFlag) {
      if (window.noteFlag) {
        window.noteFlag = 0;
        document.body.removeChild(document.getElementById("help"));
      } else {
        window.helpFlag = 0;
        document.getElementById("helpIMG2").style.background = " rgba(255, 255, 255, 0.2)";

      }
    } else
      window.location.href = "screen.html";
  } else if (musicMenuIndex == 1 || musicMenuIndex == 2) {
    musicMenuIndex = 0;
    window.location.href = "music-funct.html";
  } else if (musicMenuIndex == 3) {
    document.getElementById("playlist").style.display = "none";

    if (document.getElementById('lyrics').style.display === "none")
      window.musicMenuIndex = 0;

    if (document.getElementById('selectScreen').style.display === "none")
      window.musicMenuIndex = 1;

    var img = document.createElement("IMG");
    img.setAttribute('onclick', 'toogleHelp()');
    img.setAttribute('class', 'helpimg');
    img.id = 'helpIMG2';
    img.src = 'information.png';
    document.body.appendChild(img);
  } else if (musicMenuIndex == 4) {
    document.getElementById('poolMenu').style.display = "none";

    if (document.getElementById('lyrics').style.display === "none")
      window.musicMenuIndex = 0;

    if (document.getElementById('selectScreen').style.display === "none")
      window.musicMenuIndex = 1;

    var img = document.createElement("IMG");
    img.setAttribute('onclick', 'toogleHelp()');
    img.setAttribute('class', 'helpimg');
    img.id = 'helpIMG2';
    img.src = 'information.png';
    document.body.appendChild(img);
  }
  else if (window.musicMenuIndex == 24) {
  		if (document.getElementById('lyrics1').style.display != 'none')
			document.getElementById('lyrics1').style.display = 'none';
		else if (document.getElementById('lyrics2').style.display != 'none')
			document.getElementById('lyrics2').style.display = 'none';
		else if (document.getElementById('lyrics3').style.display != 'none')
			document.getElementById('lyrics3').style.display = 'none';

    document.getElementById('stages').style.display = 'block';
  	  window.musicMenuIndex = 2;

  }
  return;
}

function goToLyrics() {

  document.getElementById("selectScreen").style.display = "none";
  document.getElementById("lyrics").style.display = "block";
  window.musicMenuIndex = 1;
}

function saveChoice(a) {

  if (playlistArray[a] == 0)
    playlistArray[a] = 1;
  if (playlistArray[a] == 1)
    playlistArray[a] = 0;
}

/*function clickEvent () {
	document.getElementById('playlist').style.display = 'none';
	document.getElementById('selectScreen').onclick = function() {};
}*/


function showPlaylists() {

  document.getElementById("playlist").style.display = "block";
  /*document.getElementById("selectScreen").setAttribute("onclick", "clickEvent()"); */

  var i;
  for (i = 0; i < 4; i++) {
    if (playlistArray[i] == 1)
      document.getElementById("myCheck" + i).checked = true;
  }
  window.musicMenuIndex = 3;
}

function goToStages() {

  document.getElementById("selectScreen").style.display = "none";
  document.getElementById("stages").style.display = "block";
  window.musicMenuIndex = 2;
}

function clickButton(button) {

  button.style.background = gradients[1];

  if (musicMenuButton != null) {

    if (musicMenuButton == 0) {
      if (window.helpFlag)
        doHelp(0);
      else {
        document.body.removeChild(document.getElementById("helpIMG2"));
        goToLyrics();
      }

      return;
    } else if (musicMenuButton == 1) {
      if (window.helpFlag)
        doHelp(1);
      else {
        document.body.removeChild(document.getElementById("helpIMG2"));
        goToStages();
      }
    } else if (musicMenuButton == 2) {
      if (window.helpFlag)
        doHelp(2);
      else {
        document.body.removeChild(document.getElementById("helpIMG2"));
        showPlaylists();
      }
    } else if (musicMenuButton == 3) {
      if (window.helpFlag)
        doHelp(0);
      else {
        document.body.removeChild(document.getElementById("helpIMG2"));
        showPoll();
      }
    }
  }
}

function showPoll() {

  window.musicMenuIndex = 4;
  document.getElementById('poolMenu').style.display = "block";
}


function createNoteMusic() {

  var para = document.createElement("P");
  para.setAttribute("id", "notification");
  var t = document.createTextNode('The song you voted for will play sortly');
  para.appendChild(t);
  document.body.appendChild(para);

  para.setAttribute('onclick', 'document.body.removeChild(document.getElementById("notification"));');

  setTimeout(function() {
    document.body.removeChild(para);
  }, 5000);
}

function createNotificationMusic() {

  var para = document.createElement("img");
  para.src = 'notification.png';
  para.id = 'notificationIMG';
  para.setAttribute("onclick", "createNoteMusic()");

  document.body.appendChild(para);

  setTimeout(function() {
    document.body.removeChild(para);
  }, 5000);
}


function voteSong(object) {

  var table, cell, input;

  table = document.getElementById('poolTable');
  for (var i = 1; i < table.rows.length; i++) {
    input = table.rows[i].cells[1].getElementsByTagName('input')[0];
    if (input != object) {
      input.style.visibility = 'hidden';
    }
  }

  object.removeAttribute("onclick");
  object.setAttribute("onclick", "unvoteSong(this)");

  setTimeout(function() {
    document.getElementById('poolMenu').style.display = 'none';
  }, 2000);

  setTimeout(createNotificationMusic, 5000);
}

function unvoteSong(object) {

  var table, cell, input;

  table = document.getElementById('poolTable');
  for (var i = 1; i < table.rows.length; i++) {
    input = table.rows[i].cells[1].getElementsByTagName('input')[0];
    if (input != object) {
      input.style.visibility = 'visible';
    }
  }

  object.removeAttribute("onclick");
  object.setAttribute("onclick", "voteSong(this)");
}

function toggleMap(functionCall, element, number) {

  var div = document.getElementById('map');

  if (window.mapDisplay == 0) {
    window.mapDisplay = 1;
    eventManager();
    div.style.visibility = "visible";
    div.classList.remove('hide');
    div.classList.add('show');
    functionCall(element, number);
  } else {
    functionCall(element, number);
    if (window.mapStages == 0) {
      window.mapDisplay = 0;
      eventManager();
      div.classList.remove('show');
      div.classList.add('hide');
      setTimeout(function() {
        div.style.visibility = 'hidden';
      }, 1000);
    }
  }
}

function showStage(image, number) {

  var pin;
  var div = document.getElementById('map');

  if (image.style.opacity == 1 || image.style.opacity == "") {
    image.style.opacity = 0.5;
    window.mapStages++;
    pin = document.createElement("IMG");
    pin.src = image.src;
    pin.id = number;
    pin.setAttribute("class", "stage-pin");
    pin.style.top = stagecoords[number].top;
    pin.style.left = stagecoords[number].left;

    div.appendChild(pin);
  } else {
    window.mapStages--;
    image.style.opacity = 1;
    pin = document.getElementById('' + number + '');
    div.removeChild(pin);
  }
}

function eventManager() {

  if (window.mapDisplay) {
    window.addEventListener('keyup', keyUp);
    window.addEventListener('keydown', keyDown);
  } else {
    window.removeEventListener('keyup', keyUp);
    window.removeEventListener('keydown', keyDown);
  }
}

function keyUp(event) {

  if (!(event.keyCode in keypressed)) {
    return;
  } else {
    keypressed[event.keyCode] = false;
  }
}

function keyDown(event) {

  var keycode = event.keyCode;
  var img = document.getElementById('person');

  if (!(event.keyCode in keypressed)) {
    return;
  }

  keypressed[event.keyCode] = true;

  if (keypressed[87] && keypressed[65]) {
    img.style.transform = 'rotate(-90deg)'; /* W & A */
    moveDiagonal(img, -1, -1);
  } else if (keypressed[87] && keypressed[68]) {
    img.style.transform = 'rotate(0deg)'; /* W & D */
    moveDiagonal(img, -1, 1);
  } else if (keypressed[83] && keypressed[65]) {
    img.style.transform = 'rotate(180deg)'; /* S & A */
    moveDiagonal(img, 1, -1);
  } else if (keypressed[83] && keypressed[68]) {
    img.style.transform = 'rotate(90deg)'; /* S & D*/
    moveDiagonal(img, 1, 1);
  } else if (keypressed[65]) {
    img.style.transform = 'rotate(-135deg)'; /* A */
    moveHorizontal(img, -1);
  } else if (keypressed[87]) {
    img.style.transform = 'rotate(-45deg)'; /* W */
    moveVertical(img, -1);
  } else if (keypressed[68]) {
    img.style.transform = 'rotate(45deg)'; /* D */
    moveHorizontal(img, 1);
  } else if (keypressed[83]) {
    img.style.transform = 'rotate(135deg)'; /* S */
    moveVertical(img, 1);
  }
}

function moveHorizontal(element, move) {

  var tmp = personPosition.left + move;

  personPosition.left = tmp;

  if (tmp >= 5 && tmp <= 235) {
    element.style.left = '' + tmp + 'pt';
  }
}

function moveVertical(element, move) {

  var tmp = personPosition.top + move;

  personPosition.top = tmp;

  if (tmp >= 10 && tmp <= 130) {
    element.style.top = '' + tmp + 'pt';
  }
}

function moveDiagonal(element, top, left) {

  var tmp1 = personPosition.top + top;
  var tmp2 = personPosition.left + left;

  personPosition.top = tmp1;
  personPosition.left = tmp2;

  if (tmp1 >= 10 && tmp1 <= 130 && tmp2 >= 5 && tmp2 <= 235) {
    element.style.top = '' + tmp1 + 'pt';
    element.style.left = '' + tmp2 + 'pt';
  }
}
